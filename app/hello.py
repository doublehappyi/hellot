import os
import tornado.web
import tornado.httpserver
import tornado.ioloop
import tornado.options


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("hello tornado")


if __name__ == "__main__":
    app = tornado.web.Application(handlers=[("/", IndexHandler)])
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(80)
    tornado.ioloop.IOLoop.instance().start()
